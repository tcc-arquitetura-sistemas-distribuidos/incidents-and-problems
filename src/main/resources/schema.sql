drop table if exists incidents;
drop table if exists nonconformities;

CREATE TABLE nonconformities (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(100) NOT NULL,
  description VARCHAR(1000) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE incidents (
  id INT NOT NULL AUTO_INCREMENT,
  description VARCHAR(1000) NOT NULL,
  created_at DATETIME NOT NULL,
  nonconformity_id INT,
  PRIMARY KEY (id),
  FOREIGN KEY (nonconformity_id) REFERENCES nonconformities(id)
);