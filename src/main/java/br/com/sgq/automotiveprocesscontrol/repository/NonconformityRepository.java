package br.com.sgq.automotiveprocesscontrol.repository;

import br.com.sgq.automotiveprocesscontrol.model.NonconformityModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NonconformityRepository extends CrudRepository<NonconformityModel, Long> {
}
