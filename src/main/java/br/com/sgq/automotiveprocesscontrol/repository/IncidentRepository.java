package br.com.sgq.automotiveprocesscontrol.repository;

import br.com.sgq.automotiveprocesscontrol.model.IncidentModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IncidentRepository extends CrudRepository<IncidentModel, Long> {
}
