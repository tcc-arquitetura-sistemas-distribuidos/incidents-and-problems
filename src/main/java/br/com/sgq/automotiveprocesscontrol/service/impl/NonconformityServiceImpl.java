package br.com.sgq.automotiveprocesscontrol.service.impl;

import br.com.sgq.automotiveprocesscontrol.model.NonconformityModel;
import br.com.sgq.automotiveprocesscontrol.repository.NonconformityRepository;
import br.com.sgq.automotiveprocesscontrol.service.NonconformityService;
import java.util.function.Function;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class NonconformityServiceImpl implements NonconformityService {

  private final NonconformityRepository nonconformityRepository;

  public NonconformityServiceImpl(
      NonconformityRepository nonconformityRepository) {
    this.nonconformityRepository = nonconformityRepository;
  }

  @Override
  public Iterable<NonconformityModel> all() {
    return nonconformityRepository.findAll();
  }

  @Override
  public NonconformityModel create(NonconformityModel nonconformity) {
    nonconformityRepository.save(nonconformity);
    return nonconformity;
  }

  @Override
  public NonconformityModel getById(Long nonconformityId) {
    return nonconformityRepository.findById(nonconformityId)
        .orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public NonconformityModel updateById(Long id, NonconformityModel newNonconformity) {
    return nonconformityRepository.findById(id)
        .map(updateNonconformity(newNonconformity))
        .orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public NonconformityModel delete(Long id) {
    return nonconformityRepository.findById(id)
        .map(this::deleteNonconformity)
        .orElseThrow(ResourceNotFoundException::new);
  }

  private NonconformityModel deleteNonconformity(NonconformityModel nonconformity) {
    nonconformityRepository.delete(nonconformity);

    return nonconformity;
  }

  private Function<NonconformityModel, NonconformityModel> updateNonconformity(NonconformityModel newNonconformity) {
    return (nonconformity) -> {
      nonconformity.setName(newNonconformity.getName());
      nonconformity.setDescription(newNonconformity.getDescription());

      return nonconformityRepository.save(nonconformity);
    };
  }
}
