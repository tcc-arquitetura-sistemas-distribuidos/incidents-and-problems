package br.com.sgq.automotiveprocesscontrol.service;

import br.com.sgq.automotiveprocesscontrol.model.NonconformityModel;

public interface NonconformityService {

  Iterable<NonconformityModel> all();

  NonconformityModel create(NonconformityModel nonconformity);

  NonconformityModel getById(Long nonconformityId);

  NonconformityModel updateById(Long id, NonconformityModel nonconformity);

  NonconformityModel delete(Long id);
}
