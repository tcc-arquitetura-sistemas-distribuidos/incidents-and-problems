package br.com.sgq.automotiveprocesscontrol.service.impl;

import br.com.sgq.automotiveprocesscontrol.model.IncidentModel;
import br.com.sgq.automotiveprocesscontrol.model.NonconformityModel;
import br.com.sgq.automotiveprocesscontrol.queue.sender.IncidentSender;
import br.com.sgq.automotiveprocesscontrol.repository.IncidentRepository;
import br.com.sgq.automotiveprocesscontrol.repository.NonconformityRepository;
import br.com.sgq.automotiveprocesscontrol.service.IncidentService;
import java.text.MessageFormat;
import java.util.function.Function;
import java.util.function.Supplier;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class IncidentServiceImpl implements IncidentService {

  private final IncidentRepository incidentRepository;
  private final NonconformityRepository nonconformityRepository;
  private final IncidentSender incidentSender;

  public IncidentServiceImpl(
      IncidentRepository incidentRepository,
      NonconformityRepository nonconformityRepository,
      IncidentSender incidentSender) {
    this.incidentRepository = incidentRepository;
    this.nonconformityRepository = nonconformityRepository;
    this.incidentSender = incidentSender;
  }

  @Override
  public Iterable<IncidentModel> all() {
    return incidentRepository.findAll();
  }

  @Override
  public IncidentModel create(IncidentModel incident) {
    incidentRepository.save(incident);

    incidentSender.sendIncidentToQueue(incident);

    return incident;
  }

  @Override
  public IncidentModel getById(Long incidentId) {
    return incidentRepository.findById(incidentId)
        .orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public IncidentModel updateById(Long id, IncidentModel newIncident) {
    return incidentRepository.findById(id)
        .map(updateIncident(newIncident))
        .orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public IncidentModel delete(Long id) {
    return incidentRepository.findById(id)
        .map(this::deleteIncident)
        .orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public IncidentModel updateByIdIncidentNonconformity(Long incidentId, NonconformityModel nonconformity) {
    nonconformityRepository.findById(nonconformity.getId())
        .orElseThrow(resourceNotFoundException("Nonconformity", nonconformity.getId()));

    return incidentRepository.findById(incidentId)
        .map(updateIncidentNonconformity(nonconformity))
        .orElseThrow(resourceNotFoundException("Incident", incidentId));
  }

  private IncidentModel deleteIncident(IncidentModel incident) {
    incidentRepository.delete(incident);

    return incident;
  }

  private Function<IncidentModel, IncidentModel> updateIncident(IncidentModel newIncident) {
    return (incident) -> {
      incident.setDescription(newIncident.getDescription());
      incident.setCreatedAt(newIncident.getCreatedAt());

      return incidentRepository.save(incident);
    };
  }

  private Function<IncidentModel, IncidentModel> updateIncidentNonconformity(NonconformityModel newNonconformity) {
    return (incident) -> {
      incident.setNonconformity(newNonconformity);

      return incidentRepository.save(incident);
    };
  }

  private Supplier resourceNotFoundException(String resource, Long id) {
    return () -> new ResourceNotFoundException(
        MessageFormat.format("{0} Resource with id {1} not found", resource, id)
    );
  }
}
