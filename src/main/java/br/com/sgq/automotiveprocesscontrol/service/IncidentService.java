package br.com.sgq.automotiveprocesscontrol.service;

import br.com.sgq.automotiveprocesscontrol.model.IncidentModel;
import br.com.sgq.automotiveprocesscontrol.model.NonconformityModel;

public interface IncidentService {

  Iterable<IncidentModel> all();

  IncidentModel create(IncidentModel incident);

  IncidentModel getById(Long incidentId);

  IncidentModel updateById(Long id, IncidentModel newIncident);

  IncidentModel delete(Long id);

  IncidentModel updateByIdIncidentNonconformity(Long incidentId, NonconformityModel nonconformity);
}
