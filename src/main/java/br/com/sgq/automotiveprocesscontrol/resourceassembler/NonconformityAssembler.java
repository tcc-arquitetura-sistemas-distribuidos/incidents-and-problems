package br.com.sgq.automotiveprocesscontrol.resourceassembler;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import br.com.sgq.automotiveprocesscontrol.controller.NonconformityController;
import br.com.sgq.automotiveprocesscontrol.model.NonconformityModel;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class NonconformityAssembler implements ResourceAssembler<NonconformityModel, Resource<NonconformityModel>> {
  @Override
  public Resource<NonconformityModel> toResource(NonconformityModel nonconformity) {

    return new Resource<>(
        nonconformity,
        linkTo(methodOn(NonconformityController.class).byId(nonconformity.getId())).withSelfRel(),
        linkTo(methodOn(NonconformityController.class).all()).withRel("nonconformities")
    );
  }
}