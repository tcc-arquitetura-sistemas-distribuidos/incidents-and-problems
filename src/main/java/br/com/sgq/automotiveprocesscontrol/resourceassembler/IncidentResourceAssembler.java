package br.com.sgq.automotiveprocesscontrol.resourceassembler;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import br.com.sgq.automotiveprocesscontrol.controller.IncidentController;
import br.com.sgq.automotiveprocesscontrol.model.IncidentModel;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class IncidentResourceAssembler implements ResourceAssembler<IncidentModel, Resource<IncidentModel>> {
  @Override
  public Resource<IncidentModel> toResource(IncidentModel incident) {

    return new Resource<>(
        incident,
        linkTo(methodOn(IncidentController.class).byId(incident.getId())).withSelfRel(),
        linkTo(methodOn(IncidentController.class).all()).withRel("incidents")
    );
  }
}