package br.com.sgq.automotiveprocesscontrol.queue.sender;

import br.com.sgq.automotiveprocesscontrol.model.IncidentModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class IncidentSender {

  private final RabbitTemplate rabbitTemplate;
  private final String incidentQueue;

  public IncidentSender(RabbitTemplate rabbitTemplate, @Value("${queue.incident}") String incidentQueue) {
    this.rabbitTemplate = rabbitTemplate;
    this.incidentQueue = incidentQueue;
  }

  public void sendIncidentToQueue(IncidentModel incident) {
    this.rabbitTemplate.convertAndSend(this.incidentQueue, incident);
    log.info("Sent incident {} to queue.", incident.getId());
  }
}
