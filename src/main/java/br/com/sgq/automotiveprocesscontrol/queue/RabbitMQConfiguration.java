package br.com.sgq.automotiveprocesscontrol.queue;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfiguration {

  private final String exchange;

  private final String incidentQueue;

  public RabbitMQConfiguration(
      @Value("${spring.rabbitmq.template.exchange}") String exchange,
      @Value("${queue.incident}") String incidentQueue) {
    this.exchange = exchange;
    this.incidentQueue = incidentQueue;
  }

  @Bean("exchange")
  DirectExchange exchange() {
    return (DirectExchange) ExchangeBuilder.directExchange(exchange).build();
  }

  @Bean("incidentQueue")
  public Queue incidentQueue() {
    return QueueBuilder.durable(this.incidentQueue).build();
  }

  @Bean
  Binding bindingIncidentQueue(
      @Qualifier("incidentQueue") Queue queue,
      @Qualifier("exchange") DirectExchange exchange
  ) {
    return BindingBuilder.bind(queue).to(exchange).with(queue.getName());
  }

  @Bean
  public MessageConverter jsonMessageConverter() {
    return new Jackson2JsonMessageConverter();
  }
}
