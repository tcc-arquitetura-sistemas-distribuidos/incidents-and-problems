package br.com.sgq.automotiveprocesscontrol.controller;

import br.com.sgq.automotiveprocesscontrol.model.IncidentModel;
import br.com.sgq.automotiveprocesscontrol.model.NonconformityModel;
import br.com.sgq.automotiveprocesscontrol.resourceassembler.IncidentResourceAssembler;
import br.com.sgq.automotiveprocesscontrol.service.IncidentService;
import java.net.URI;
import java.net.URISyntaxException;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/incidents")
public class IncidentController {

  private final IncidentService incidentService;
  private final IncidentResourceAssembler assembler;

  public IncidentController(
      IncidentService incidentService,
      IncidentResourceAssembler assembler
  ) {
    this.incidentService = incidentService;
    this.assembler = assembler;
  }

  @GetMapping
  public ResponseEntity all() {
    return ResponseEntity.ok(incidentService.all());
  }

  @PostMapping
  public ResponseEntity create(@RequestBody IncidentModel incident) throws URISyntaxException {
    IncidentModel createdIncident = incidentService.create(incident);

    Resource<IncidentModel> resource = assembler.toResource(createdIncident);

    return ResponseEntity.created(new URI(resource.getId().expand().getHref()))
        .body(createdIncident);
  }

  @GetMapping(path = "/{id}")
  public ResponseEntity byId(@PathVariable Long id) {
    return ResponseEntity.ok(incidentService.getById(id));
  }

  @PutMapping(path = "/{id}")
  public ResponseEntity updateById(@PathVariable Long id, @RequestBody IncidentModel incident) {
    return ResponseEntity.ok(incidentService.updateById(id, incident));
  }

  @DeleteMapping(path = "/{id}")
  public ResponseEntity delete(@PathVariable Long id) {
    incidentService.delete(id);

    return ResponseEntity.noContent().build();
  }

  @PutMapping(path = "/{incidentId}/nonconformity")
  public ResponseEntity updateNonconformity(@PathVariable Long incidentId, @RequestBody NonconformityModel nonconformity) {
    return ResponseEntity.ok(incidentService.updateByIdIncidentNonconformity(incidentId, nonconformity));
  }

}
