package br.com.sgq.automotiveprocesscontrol.controller;

import br.com.sgq.automotiveprocesscontrol.model.NonconformityModel;
import br.com.sgq.automotiveprocesscontrol.resourceassembler.NonconformityAssembler;
import br.com.sgq.automotiveprocesscontrol.service.NonconformityService;
import java.net.URI;
import java.net.URISyntaxException;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/nonconformities")
public class NonconformityController {

  private final NonconformityService nonconformityService;
  private final NonconformityAssembler assembler;

  public NonconformityController(NonconformityService nonconformityService,
      NonconformityAssembler assembler) {
    this.nonconformityService = nonconformityService;
    this.assembler = assembler;
  }

  @GetMapping
  public ResponseEntity all() {
    return ResponseEntity.ok(nonconformityService.all());
  }

  @PostMapping
  public ResponseEntity create(@RequestBody NonconformityModel nonconformity) throws URISyntaxException {
    NonconformityModel createdNonconformity = nonconformityService.create(nonconformity);

    Resource<NonconformityModel> resource = assembler.toResource(createdNonconformity);

    return ResponseEntity.created(new URI(resource.getId().expand().getHref()))
      .body(createdNonconformity);
  }

  @GetMapping(path = "/{id}")
  public ResponseEntity byId(@PathVariable Long id) {
    return ResponseEntity.ok(nonconformityService.getById(id));
  }

  @PutMapping(path = "/{id}")
  public ResponseEntity updateById(@PathVariable Long id, @RequestBody NonconformityModel nonconformity) {
    return ResponseEntity.ok(nonconformityService.updateById(id, nonconformity));
  }

  @DeleteMapping(path = "/{id}")
  public ResponseEntity delete(@PathVariable Long id) {
    nonconformityService.delete(id);

    return ResponseEntity.noContent().build();
  }

}
