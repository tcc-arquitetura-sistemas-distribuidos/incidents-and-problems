# Módulo - Incidents and Proccess

Para iniciar este microserviço junto com suas dependências execute o seguinte comando:

```bash
docker-compose -f src/main/resources/docker/docker-compose.yml up -d
source environments.sh
mvn clean install
java -jar target/*.jar
```

## Swagger:
Após ter executado o docker-compose acima o serviço swagger ui estará disponível neste endereço:

http://localhost:8081

---

Utilizando a ferramenta [httpie](https://httpie.org/) é possível efetuar testes nas apis rest disponíveis nesta aplicação:

```
# Listar incidents:
http get :8091/incidents-and-problems/v1/incidents

# Listar nonconformities:
http get :8091/incidents-and-problems/v1/nonconformities

# Criar uma nonconformity:
http post :8091/incidents-and-problems/v1/nonconformities < src/main/resources/mock-data/nonconformity.json

# Criar um incident:
http post :8091/incidents-and-problems/v1/incidents < src/main/resources/mock-data/incident.json

# Registrar a nonconformity de um incident:
http put :8091/incidents-and-problems/v1/incidents/1/nonconformity id=1

# Entre outras chamadas disponíveis no swagger
```